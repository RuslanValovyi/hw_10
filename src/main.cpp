#include "wine.h"
#include <iostream>

int main() {
    using std::cin;
    using std::cout;
    using std::endl;
    
    constexpr int nYears = 2;
    int yn[nYears] = {1956, 1967};
    int bn[nYears] = {15, 28};

    cout << "Enter name of wine: ";
    char lab[50] = "";
    cin.getline(lab, 50);
    Wine w(lab, nYears, yn, bn);
    w.Show();
    w.getBottles();
    cout << "Total bottles for " << w.label() << ": " << w.sum() << endl;
    cout << endl;
   
    constexpr int kYears = 3;
    int y[kYears] = { 1993, 1995, 1998 };
    int b[kYears] = { 48, 60, 72 };

    Wine more("Chianti", kYears, y, b);
    more.Show();
    more.getBottles();
    cout << "Total bottles for " << more.label() << ": " << more.sum() << endl;
    
    return 0;
}
