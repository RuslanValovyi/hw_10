#pragma once
#include <iostream>
#include<valarray>
#include <utility>
#include <cstring>
#include <algorithm>

#include <time.h>

const int MAX_YB = 10;

using ArrayIntT = std::valarray<int>;
using PairArrayT = std::pair<ArrayIntT, ArrayIntT>;

/********************************************************************************************/
class WineName {
    char* name;
public:
    WineName(const char* n = "none")
    : name{new char[std::strlen(n)]} {
        strcpy(name, n);
    }
    virtual ~WineName() { delete [] name; }
    
    char * getName() {
        return name;
    }
};

/********************************************************************************************/
class WineYB {
    PairArrayT yb;
public:
    WineYB()
    : yb(0, 0) {}
    
    WineYB(int y, const int yr[], const int bot[]) 
    : yb((yr, y), (bot, y)) 
    {
        for(int i = 0; i < yb.first.size(); i++) {
            yb.first[i] = yr[i];
            yb.second[i] = bot[i];
        }
    }
    
    WineYB(int y) {
        yb.first.resize(y);
        yb.second.resize(y);
    }
    virtual ~WineYB() { yb.first.resize(0); yb.second.resize(0); }
    
    PairArrayT getYB() {
        return yb;
    }
    int getSize() {
        return yb.first.size();
    }
    virtual void Show() {
        for(int i = 0; i < yb.first.size(); i++) {
            std::cout << yb.first[i] << " year; " << yb.second[i] << " bottles." << std::endl;
        }
    }
    int addYB(int year, int bottles) {
        if (yb.first.size() < MAX_YB && yb.second.size() < MAX_YB) {
            bool done = false;
            for(int i = 0; i < yb.first.size(); i++) {
                if (yb.first[i] == year) {
                    yb.second[i] += bottles;
                    done = true;
                    break;
                }
            }
            if (done != true) {
                yb.first.resize(yb.first.size() + 1);
                yb.second.resize(yb.second.size() + 1);
                yb.first[yb.first.size()-1] = year;
                yb.second[yb.second.size()-1] = bottles;
            }
            return 0;
        }
        return -1;
    }
};

/********************************************************************************************/
class Wine {
private:
    WineName *name;
    WineYB *yb;
public:
    Wine()
    : name{new WineName()} 
    , yb{new WineYB()} {}
    
    // l - means label, y - number of years, yr[] - years of bottling, bot[] - number of bottles
    Wine(const char* l, int y, const int yr[], const int bot[])
    : name{new WineName(l)} 
    , yb{new WineYB(y, yr, bot)} {}
    
    // l - label, y - number of years, array is created with that size
    Wine(const char* l, int y)
    : name{new WineName(l)} 
    , yb{new WineYB(y)} {}
    
    virtual ~Wine() { delete name; delete yb; }
    
    char * label() {
        return name->getName();
    }
    
    int sum() {
        return yb->getYB().second.sum();
    }
    
    int getAge(int year) {
        time_t theTime = time(NULL);
        struct tm *aTime = localtime(&theTime);
        return aTime->tm_year + 1900 - year;
    }

    virtual void Show() {
        std::cout << "Wine " << name->getName() << " age: " << std::endl;
        for(int i = 0; i < yb->getYB().first.size(); i++) {
            std::cout << yb->getYB().first[i] << " year; " << yb->getYB().second[i] << " bottles;" << getAge(yb->getYB().first[i]) << " years old;" << std::endl;
        }
        std::cout << std::endl;
    }

    int getBottles() const {
        int bottles = 0;
        std::cout << "Enter year of bottling: ";
        int yr{0};
        std::cin >> yr;
        PairArrayT r = yb->getYB();
        for(int i = 0; i < r.first.size(); i++) {
            if (r.first[i] == yr) 
                bottles += r.second[i];
        }

        std::cout << bottles << " bottles were produced in " << yr << " year." << std::endl;
        return bottles; 
    }
};
/********************************************************************************************/

