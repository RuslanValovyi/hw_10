your home task

Class Wine has a member of string type object for its name and Pair object (std::pair) which consists of two std::valarray<int> objects: 
1st one for a year of bottling, 2nd one for number of bottles produced that year. For example, the 1st object contains years 1998, 1992 and 1996, 
and the 2nd one contains 24, 48 and 144 correspondingly.

It would also be nice to have some class member for storing wine's age.

you can use following aliases for these types:
using ArrayIntT = std::valarray<int>
using PairArrayT = std::pair<ArrayIntT, ArrayIntT>

(So that PairArrayT type represents std::pair<std::valarray<int>, std::valarray<int>>)

Implement Wine class using "has-a" relationship using 1) composition 2) private inheritance. This class should have a default constructor as well as the following ones:

Wine(const char* l, int y, const int yr[], const int bot[]);
// l - means label, y - number of years, yr[] - years of bottling, bot[] - number of bottles

Wine(const char* l, int y)
// l - label, y - number of years, array is created with that size

Wine class should also have a getBottles() method, which should ask the user for the year of bottling and number of bottles for that year.

Method label() should return a link to wine's label, and sum() should return total number of bottles from the second valarray<int> object from PairArrayT.

Your application should ask the user to enter the wine's label, number of elements in array and a year and number of bottles for each array element. 
It should use this data to create a Wine object and print its information. Please see an example of test program in the attachment.